/*
1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval().
setTimeout() має 2 основні параметри, функцію (або код, але передавати код це погана практика) та час в мілісекундах. 
Викликає передану функцію один раз через задану кількість часу. Додаткові параметри це аргументи, які можна передавати у функцію.
setInterval() працює схожим чином, але буде викликати передану функцію щоразу через заданий інтервал в мілісекундах. 

2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
Код в JS виконується підряд зверху до низу. Якщо передати в setTimeout() другим аргументом 0, то виконання потрапить у кінець "черги".
Таким чином код виконається одразу після усього іншого коду, тобто у порядку "черги".

3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
setInterval() продовжує виконуватися через заданий інтервал, використовуючи ресурси. Відповідно після того, коли необхідність у ньому зникає
необхідно викликати функцію clearInterval(), щоб не навантажувати ресурси зайвим
*/

// Знаходимо колекцію картинок та їх обгортку
const imgCollection = document.querySelectorAll(".image-to-show");
const imgWrapper = document.querySelector(".images-wrapper");

// створюємо кнопки stop та continue з відповідним текстом та класами
const stopButton = document.createElement("button");
stopButton.innerText = "Stop";
stopButton.classList.add("btn", "stop");

const contButton = document.createElement("button");
contButton.innerText = "Continue";
contButton.classList.add("btn", "cont", "disabled");

// створюємо екран таймера
const timerField = document.createElement("div");
timerField.id = "timer";

const interval = 3000;

let i = 0;

// оголошуємо змінні для таймерів щоб отримати їх ID та використати потім в clearInterval
let bannerTimer;
let timerClock;

// змінна для відліку часу, що залишився
let endTime = Date.now() + interval;

// основна функція, що показує картинки по таймеру. setInterval обернений у функцію щоб можна було його викликати після clearInterval
function startBanners () {
    bannerTimer = setInterval(function () {
    // щоразу після виклику функції оновлюється endTime
        endTime = Date.now() + interval;
    // кнопки з'являються після появи другої картинки
        imgWrapper.after(contButton);
        imgWrapper.after(stopButton);
    // основний блок
        if (i < (imgCollection.length - 1)) {
            i++;
        } else {
            i = 0;
        }
            imgCollection.forEach((e) => $(e).fadeOut(500));
          setTimeout(() => $(imgCollection[i]).fadeIn(500), 500);  
    }, interval);};

// функція, що виводить таймер
    function startTimerClock () {
        timerClock = setInterval(function () {
            imgWrapper.after(timerField);
            let remainder = endTime - Date.now();
            let seconds = Math.floor(remainder / 1000);
            let milisec = (remainder + "").slice(-3);
            document.querySelector("#timer").innerHTML = `For the next image 0${seconds}s.${milisec}ms left`;
        }, 10);};

// логіка зупинення/відновлення таймерів при натисканні кнопок. Додатково блокування кнопки після її натискання
stopButton.addEventListener("click", (e) => {
    clearInterval(bannerTimer);
    clearInterval(timerClock);
    document.querySelector("#timer").innerHTML = "Timer is stopped <br/> Press continue button";
    e.target.classList.add("disabled");
    contButton.classList.remove("disabled");
});

contButton.addEventListener("click", (e) => {
    startBanners ();
    startTimerClock ();
    endTime = Date.now() + interval;
    e.target.classList.add("disabled");
    stopButton.classList.remove("disabled");
});

startBanners ();
startTimerClock ();